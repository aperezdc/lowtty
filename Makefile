#
# Makefile
# Adrian Perez de Castro, 2018-09-23 17:03
#

override CPPFLAGS += -Ideps
override CFLAGS   += -Wall -std=c11

all: lowtty

S := lowtty.c
O := $(patsubst %.c,%.o,$S)

lowtty: $O
lowtty: LDLIBS = -llowdown

clean:
	$(RM) lowtty $O
