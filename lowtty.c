/*
 * lowtty.c
 * Copyright (C) 2018 Adrian Perez de Castro <aperez@igalia.com>
 *
 * Distributed under terms of the MIT license.
 */

#define _POSIX_C_SOURCE 199802L

#include "apicheck/apicheck.h"
#include "autocleanup/autocleanup.h"

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/queue.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <lowdown.h>

#define LNODES(m)      \
    m(ROOT)            \
    m(BLOCKCODE)       \
    m(BLOCKQUOTE)      \
    m(HEADER)          \
    m(HRULE)           \
    m(LIST)            \
    m(LISTITEM)        \
    m(PARAGRAPH)       \
    m(TABLE_BLOCK)     \
    m(TABLE_HEADER)    \
    m(TABLE_BODY)      \
    m(TABLE_ROW)       \
    m(TABLE_CELL)      \
    m(FOOTNOTES_BLOCK) \
    m(FOOTNOTE_DEF)    \
    m(BLOCKHTML)       \
    m(LINK_AUTO)       \
    m(CODESPAN)        \
    m(DOUBLE_EMPHASIS) \
    m(EMPHASIS)        \
    m(HIGHLIGHT)       \
    m(IMAGE)           \
    m(LINEBREAK)       \
    m(LINK)            \
    m(TRIPLE_EMPHASIS) \
    m(STRIKETHROUGH)   \
    m(SUPERSCRIPT)     \
    m(FOOTNOTE_REF)    \
    m(MATH_BLOCK)      \
    m(RAW_HTML)        \
    m(ENTITY)          \
    m(NORMAL_TEXT)     \
    m(DOC_HEADER)      \
    m(DOC_FOOTER)


#define assert_unreachable()                                    \
    do {                                                        \
        fprintf (stderr,                                        \
                 "%s:%u: this should have been unreachable!\n", \
                 __FILE__,                                      \
                 __LINE__);                                     \
        abort ();                                               \
    } while (false)


#define length_of(arr)  (sizeof (arr) / sizeof (arr[0]))
#define auto __auto_type


typedef struct {
    int    fd;
    size_t size;
    void  *addr;
} fmap;


static void
fmap_close (fmap *fm)
{
    if (fm) {
        if (fm->addr) {
            assert (fm->fd >= 0);
            assert (fm->size > 0);

            munmap (fm->addr, fm->size);
            fm->addr = NULL;
            fm->size = 0;
        }

        handle_clear (&fm->fd, close, -1);
        free (fm);
    }
}

PTR_AUTO_DEFINE (fmap, fmap_close)


static fmap*
fmap_open (const char *path, int flags)
{
    int prot;
    switch (flags & (O_RDWR | O_RDONLY | O_WRONLY)) {
        case O_RDWR:
            prot = PROT_READ | PROT_WRITE;
            break;
        case O_RDONLY:
            prot = PROT_READ;
            break;
        case O_WRONLY:
            prot = PROT_WRITE;
            break;
        default:
            assert_unreachable ();
    }

    ptr_auto(fmap) fm = calloc (1, sizeof (fmap));

    if ((fm->fd = open (path, flags)) == -1)
        return NULL;

    struct stat sb;
    if (fstat (fm->fd, &sb) == -1)
        return NULL;
    fm->size = sb.st_size;

    fm->addr = mmap (NULL, fm->size, prot, MAP_SHARED, fm->fd, 0);
    if (fm->addr == MAP_FAILED) {
        fm->addr = NULL;  // MAP_FAILED usually is *not* NULL.
        return NULL;
    }

    return ptr_steal (&fm);
}


typedef struct lowdown_node lnode;

PTR_AUTO_DEFINE (hdoc, lowdown_doc_free)
PTR_AUTO_DEFINE (lnode, lowdown_node_free)


typedef struct {
    const char   *name;
    unsigned int  value;
} feature_map_entry;


static const feature_map_entry s_input_features[] = {
    { "autolink",   .value = LOWDOWN_AUTOLINK   },
    { "commonmark", .value = LOWDOWN_COMMONMARK },
    { "fenced",     .value = LOWDOWN_FENCED     },
    { "footnotes",  .value = LOWDOWN_FOOTNOTES  },
    { "hilite",     .value = LOWDOWN_HILITE     },
    { "math",       .value = LOWDOWN_MATH       },
    { "mathexp",    .value = LOWDOWN_MATHEXP    },
    { "metadata",   .value = LOWDOWN_METADATA   },
    { "nocodeind",  .value = LOWDOWN_NOCODEIND  },
    { "nointem",    .value = LOWDOWN_NOINTEM    },
    { "strike",     .value = LOWDOWN_STRIKE     },
    { "super",      .value = LOWDOWN_SUPER      },
    { "tables",     .value = LOWDOWN_TABLES     },
    { NULL }
};


static const feature_map_entry s_output_features[] = {
    { "smarty", .value = LOWDOWN_SMARTY },
    { NULL }
};


static bool
find_feature (const feature_map_entry features[],
              const char             *name,
              unsigned int           *value)
{
    api_check_return_val (features, false);
    api_check_return_val (name, false);
    api_check_return_val (value, false);

    for (unsigned i = 0; features[i].name != NULL; i++) {
        if (strcmp (features[i].name, name) == 0) {
            *value = features[i].value;
            return true;
        }
    }

    return false;
}


static void
log_error_to_file_stream (enum lowdown_err err,
                          void            *arg,
                          const char      *msg)
{
    if (arg == NULL)
        return;

    const char *desc = NULL;
    switch (err) {
        case LOWDOWN_ERR_DUPE_FOOTNOTE:
            desc = "Ignored reference to duplicate footnote";
            break;
        case LOWDOWN_ERR_METADATA_BAD_CHAR:
            desc = "Bad character in metadata key";
            break;
        case LOWDOWN_ERR_SPACE_BEFORE_LINK:
            desc = "Unportable spaces before URL link";
            break;
        case LOWDOWN_ERR_UNKNOWN_FOOTNOTE:
            desc = "Ignored reference to unknown footnote";
            break;
        default:
            assert_unreachable ();
    }

    assert (desc != NULL);

    fprintf ((FILE*) arg, "%s: %s.\n", desc, msg);
    fflush ((FILE*) arg);
}
    

static struct lowdown_opts s_md_opts = {
    .msg = log_error_to_file_stream,
    .feat = LOWDOWN_FENCED | LOWDOWN_TABLES | LOWDOWN_COMMONMARK,
};


static lnode*
parse_fmap (fmap *fm)
{
    api_check_return_val (fm != NULL, NULL);
    ptr_auto(hdoc) doc = lowdown_doc_new (&s_md_opts);
    return lowdown_doc_parse (doc, fm->addr, fm->size, NULL, NULL);
}


enum render_style_id {
    R_NORMAL,
    R_BOLD,
    R_HEADER1,
    R_HEADER2 = R_HEADER1 + 1,
    R_HEADER3 = R_HEADER2 + 1,
    R_HEADER4 = R_HEADER3 + 1,
    R_HEADER5 = R_HEADER4 + 1,
    R_HEADER6 = R_HEADER5 + 1,
    R_PARAGRAPH,
    R_PREFORMAT,
    R_HRULE,
    R_BLOCKCODE,
    R_LISTITEM,
    R_IMAGE,

    R_CHILDREN,  // Traverse all the child nodes and render them.
    R_SKIP,      // Do not render, nor traverse child nodes.
    R_UNKNOWN,   // Emit a warning, do not traverse child nodes.
};


static inline enum render_style_id
lnode_to_render_style_id (const lnode* const node)
{
    api_check_return_val (node != NULL, R_UNKNOWN);

    switch (node->type) {
        case LOWDOWN_HEADER:
            return R_HEADER1 + node->rndr_header.level - 1;
        case LOWDOWN_NORMAL_TEXT:
            return R_NORMAL;
        case LOWDOWN_PARAGRAPH:
            return R_PARAGRAPH;
        case LOWDOWN_LISTITEM:
            return R_LISTITEM;
        case LOWDOWN_EMPHASIS:
            return R_BOLD;
        case LOWDOWN_CODESPAN:
            return R_PREFORMAT;
        case LOWDOWN_IMAGE:
            return R_IMAGE;
        case LOWDOWN_BLOCKCODE:
            return R_BLOCKCODE;
        case LOWDOWN_ROOT:
        case LOWDOWN_LIST:
        case LOWDOWN_LINK:  // TODO: Gather links.
            return R_CHILDREN;
        case LOWDOWN_DOC_HEADER:
        case LOWDOWN_DOC_FOOTER:
            return R_SKIP;
        default:
            return R_UNKNOWN;
    }
}


typedef struct {
    FILE *output;
    enum render_style_id style_id;
    unsigned line;
    unsigned cr : 1;
    unsigned spc : 1;
} render_state;


typedef struct {
    const char *csi;
    void (*render) (render_state*, const lnode* const);
} style;


static inline const char*
node_type_name (const lnode* const node)
{
#define LNODE_ITEM(name) \
        case LOWDOWN_ ## name: \
            return #name;

    switch (node->type) {
        LNODES (LNODE_ITEM)
        default:
            return "<UNKNOWN>";
    }

#undef LNODE_ITEM
}


static inline void
render_char (render_state* state, int c)
{
    if (c == '\n') {
        state->spc = state->cr = true;
        state->line++;
    } else if (isspace (c)) {
        state->spc = true;
        state->cr = false;
    } else {
        state->spc = state->cr = false;
    }

    fputc (c, state->output);
}

static inline void
render_cr (render_state* state, bool force)
{
    if (force || !state->cr)
        render_char (state, '\n');
}


static void render_node (render_state* state, const lnode* const node);

static inline void
render_child_nodes (render_state* state, const lnode* const node)
{
    const lnode* child;
    TAILQ_FOREACH (child, &node->children, entries)
        render_node (state, child);
}

static inline bool
is_empty_hbuf (const hbuf* const text)
{
    return !text || text->size == 0;
}

static inline void
render_hbuf (render_state* state, const hbuf* const text, bool literal)
{
    if (is_empty_hbuf (text))
        return;

    for (size_t i = 0; i < text->size; i++) {
        const auto c = text->data[i];
        if (!literal && isspace (c) && !state->spc) {
            render_char (state, ' ');
        } else {
            render_char (state, c);
        }
    }
}

static void
render_text_node (render_state* state, const lnode* const node)
{
    assert (TAILQ_EMPTY (&node->children));

    const hbuf* text = NULL;
    switch (node->type) {
        case LOWDOWN_NORMAL_TEXT:
            text = &node->rndr_normal_text.text;
            break;
        case LOWDOWN_CODESPAN:
            text = &node->rndr_codespan.text;
            break;
        default:
            fprintf (stderr, "[Cannot render %s as text]", node_type_name (node));
            return;
    }

    render_hbuf (state, text, node->type == LOWDOWN_CODESPAN);
}

static void
render_header_node (render_state* state, const lnode* const node)
{
    if (state->line > 1)
        render_cr (state, true);

    for (unsigned i = 0; i < node->rndr_header.level; i++)
        render_char (state, '#');
    render_char (state, ' ');

    render_child_nodes (state, node);
    render_cr (state, false);
}

static void
render_paragraph_node (render_state* state, const lnode* const node)
{
    render_cr (state, true);
    render_child_nodes (state, node);
    render_cr (state, true);
}

static void
render_list_item_node (render_state* state, const lnode* const node)
{
    render_cr (state, true);

    // TODO: Indentation.
    render_char (state, '*');
    render_char (state, ' ');

    render_child_nodes (state, node);
    render_cr (state, false);
}

static void
render_image_node (render_state* state, const lnode* const node)
{
    const auto text = is_empty_hbuf (&node->rndr_image.alt)
        ? (is_empty_hbuf (&node->rndr_image.title)
           ? NULL
           : &node->rndr_image.title)
        : &node->rndr_image.alt;

    render_char (state, '[');
    if (text)
        render_hbuf (state, text, false);
    else
        fputs ("IMG", state->output);
    render_char (state, ']');
}

static void
render_blockcode_node (render_state* state, const lnode* const node)
{
    // TODO: Syntax-highlight the block of code.
    render_cr (state, true);
    render_hbuf (state, &node->rndr_blockcode.text, true);
    render_cr (state, false);
}

static void
render_warn_node (render_state* state, const lnode* const node)
{
    fprintf (stderr, "[No renderer for %s nodes]", node_type_name (node));
}

static const style s_render_style[] =
{
    [R_NORMAL]    = { .render = render_text_node },
    [R_BOLD]      = { .csi = "1;1" },
    [R_HEADER1]   = { .render = render_header_node, .csi = "31;2" },
    [R_HEADER2]   = { .render = render_header_node, .csi = "31;3" },
    [R_HEADER3]   = { .render = render_header_node, .csi = "31;4" },
    [R_HEADER4]   = { .render = render_header_node },
    [R_HEADER5]   = { .render = render_header_node },
    [R_HEADER6]   = { .render = render_header_node },
    [R_HRULE]     = { .csi = "0;30" },
    [R_BLOCKCODE] = { .render = render_blockcode_node, .csi = "1;30" },
    [R_IMAGE]     = { .render = render_image_node, .csi = "2;44" },
    [R_PREFORMAT] = { .render = render_text_node, .csi = "1;40" },
    [R_PARAGRAPH] = { .render = render_paragraph_node },
    [R_LISTITEM ] = { .render = render_list_item_node },
    [R_CHILDREN ] = { .render = render_child_nodes },
    [R_UNKNOWN  ] = { .render = render_warn_node },
    [R_SKIP     ] = { },
};


static void
render_node (render_state* state, const lnode *node)
{
    api_check_return (node);

    const auto prev_style_id = state->style_id;
    state->style_id = lnode_to_render_style_id (node);

    assert (state->style_id < length_of (s_render_style));
    const auto style = &s_render_style[state->style_id];

    if (state->style_id != prev_style_id) {
        if (style->csi)
            fprintf (state->output, "\033[%sm", style->csi);
    }

    if (style->render)
        (*style->render) (state, node);

    if (state->style_id != prev_style_id) {
        if (style->csi)
            fputs ("\033[0;0m", state->output);

        state->style_id = prev_style_id;
    }
}


static void
help (const char *argv0, FILE* output)
{
    api_check_return (argv0);
    api_check_return (output);

    fprintf (output,
             "usage: %s [-v] [-D feature] [-d feature] [-E feature] [-e feature] [file]\n",
             argv0);
}


static void
err_exit (const char *argv0,
          const char *format,
          ...)
{
    api_check_return (argv0);
    api_check_return (format);

    fprintf (stderr, "%s: ", argv0);

    va_list args;
    va_start (args, format);
    vfprintf (stderr, format, args);
    va_end (args);

    fflush (stderr);

    exit (EXIT_FAILURE);
}


int
main (int argc, char *argv[])
{
    int opt;
    unsigned int feature;

    while ((opt = getopt (argc, argv, "hD:d:E:e:v")) != -1) {
        switch (opt) {
            case 'h':
                help (argv[0], stdout);
                return EXIT_SUCCESS;

            case 'v':
                s_md_opts.arg = stderr;
                break;

            case 'D':
                if (!find_feature (s_output_features, optarg, &feature))
                    err_exit (argv[0], "Invalid feature name: %s.\n", optarg);
                s_md_opts.oflags &= ~feature;
                break;

            case 'd':
                if (!find_feature (s_input_features, optarg, &feature))
                    err_exit (argv[0], "Invalid feature name: %s.\n", optarg);
                s_md_opts.feat &= ~feature;
                break;

            case 'E':
                if (!find_feature (s_output_features, optarg, &feature))
                    err_exit (argv[0], "Invalid feature name: %s.\n", optarg);
                s_md_opts.oflags |= feature;
                break;

            case 'e':
                if (!find_feature (s_input_features, optarg, &feature))
                    err_exit (argv[0], "Invalid feature name: %s.\n", optarg);
                s_md_opts.feat |= feature;
                break;

            case '?':
                help (argv[0], stderr);
                return EXIT_FAILURE;
        }
    }

    const int nfiles = (argc - optind);
    if (nfiles != 1)
        err_exit (argv[0], "please specify one file in the command line.\n");

    ptr_auto(lnode) document = NULL;

    if (nfiles == 1) {
        ptr_auto(fmap) fm = fmap_open (argv[optind], O_RDONLY);
        if (!fm)
            err_exit (argv[0], "Cannot open file '%s': %s.\n",
                      argv[optind], strerror (errno));
        document = parse_fmap (fm);
    } else {
        err_exit (argv[0], "Reading from standard input is unsupported.\n");
    }

    render_state state = {
        .output = stdout,
        .style_id = R_UNKNOWN,
        .cr = true,
        .spc = false,
        .line = 1,
    };
    render_node (&state, document);

    return EXIT_SUCCESS;
}
